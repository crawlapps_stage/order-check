<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    protected $fillable = ['order_id', "topic", "user_id", "data", "created_at", "updated_at","id", "is_executed"];
}
