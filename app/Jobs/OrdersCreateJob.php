<?php namespace App\Jobs;

use App\Events\CheckOrder;
use App\LineItems;
use App\Models\Order;
use App\Models\Webhook;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use stdClass;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var stdClass
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain
     * @param stdClass $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::Info("=============== Order Create Webhook ==================");
        try {
//            $shopDomain = $this->shopDomain->toNative();
//            \Log::info(\Auth::user());
//            $user = User::where('name', $shopDomain)->first();
//            \Log::info($user);
//            $user_id = ( $user ) ? $user->id : '';
//            $order = json_encode($this->data);
//            $order_id = $this->data->id;
//            $entity = Webhook::updateOrCreate(
//                ['order_id' => $order_id, 'user_id' => $user_id],
//                ['order_id' => $order_id, 'topic' => 'orders/create', 'user_id' => $user_id, 'data' => $order, 'is_executed' => 0]
//            );
//            event(new CheckOrder($entity->id, $user_id, $order_id));
//            if( !empty($sh_order->note_attributes) ) {
//                foreach ($sh_order->note_attributes as $key => $val) {
//                    if ($val->name == 'date') {
//                        if( is_array($sh_order->line_items) && !empty($sh_order->line_items) ){
//                            $counter = count($sh_order->line_items);
//                        }else{
//                            $counter = 0;
//                        }
//
//                        $order = new Order;
//                        $order->user_id = $user_id;
//                        $order->order_id = $sh_order->id;
//                        $order->order_number = $sh_order->order_number;
//                        $order->order_name = $sh_order->name;
//                        $order->order_total = $sh_order->total_price;
//                        $order->order_counter = $counter;
//                        $order->date = $val->value;
//                        $order->save();
//                    }
//                }
//                if( !empty( $sh_order->line_items ) ){
//                    foreach ( $sh_order->line_items as $lkey=>$lval ){
////                        \Log::Info(json_encode($lval));
//                        if( $lval->product_id ){
//                            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'. $lval->product_id .'.json';
//                            $parameter['fields'] = "id,product_type";
//
//                            $products = $user->api()->rest('GET', $endPoint, $parameter);
//                            \Log::info(json_encode($products));
//                            if( !$products->errors ){
//                                if ($products->body->product) {
//                                    $product = $products->body->product;
//                                    $product_type = $product->product_type;
//                                    \Log::info($product_type);
//                                }
//                            }
//                        }
//
//                        if( is_array($lval->properties) && !empty($lval->properties) ){
//                            foreach ( $lval->properties as $pkey=>$pval ){
//                                $properties[] = $pval->name . ': ' . $pval->value;
//                            }
//                        }else{
//                            $properties = '';
//                        }
//
//                        $lineitems = new LineItems;
//                        $lineitems->user_id = $user_id;
//                        $lineitems->lineitem_id = $lval->id;
//                        $lineitems->order_id = $sh_order->id;
//                        $lineitems->product_id = $lval->product_id;
//                        $lineitems->variant_id = $lval->variant_id;
//                        $lineitems->order_name = $sh_order->name;
//                        $lineitems->product_name = $lval->title;
//                        $lineitems->product_type = $product_type;
//                        $lineitems->variant_name = $lval->variant_title;
//                        $lineitems->quantity = $lval->quantity;
//                        $lineitems->price = $lval->price;
//                        $lineitems->total = (int) $lval->quantity * (float) $lval->price;
//                        $lineitems->properties = ( is_array($properties) ) ? implode( " , ", $properties ) : '';
//                        $lineitems->date = $this->date;
//                        $lineitems->save();
//                    }
//                }
//            }
        }catch(\Exception $e){
            \Log::Info('=============== ERROR:: Order Create ==================');
            \Log::Info(json_encode($e));
        }
    }
}
