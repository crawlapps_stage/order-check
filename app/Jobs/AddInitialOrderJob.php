<?php

namespace App\Jobs;

use App\LineItems;
use App\Models\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddInitialOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $shop_id = '';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->shop_id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            \Log::info('================= After Authenticate Job ==================');

            $shop = User::where('id', $this->shop_id)->first();
            $endPoint = '/admin/api/'. env('SHOPIFY_API_VERSION') .'/orders/count.json';
            $result = $shop->api()->rest('GET', $endPoint);
            \Log::info(json_encode($result));
            $count = $result->body->count;
            \Log::info('count ::' . $count);
            $total_page = number_format(ceil($count / 250), 0);

            \Log::info('total_page :: ' . $total_page);
            for($i=1; $i <= $total_page; $i++ ){
                \Log::info($i);
                $parameter['limit'] = 250;
                $parameter['page'] = $i;
                $endPoint = '/admin/api/'. env('SHOPIFY_SEARCH_API_VERSION') .'/orders.json';
                $result = $shop->api()->rest('GET', $endPoint, $parameter);
                if( !$result->errors ){
                    $sh_orders = $result->body->orders;
                    foreach($sh_orders as $orderk=>$orderv){
                        if( !empty($orderv->note_attributes)){
                            \Log::info(json_encode($orderv->note_attributes));
                            foreach ($orderv->note_attributes as $key=>$val){
                                if( $val->name == 'date' ){
                                    $this->date = $val->value;
                                    $o = Order::where('order_id', $orderv->id)->where('user_id', $shop->id)->first();

                                    if( is_array($orderv->line_items) && !empty($orderv->line_items) ){
                                        $counter = count($orderv->line_items);
                                    }else{
                                        $counter = 0;
                                    }
                                    $name = '';
                                    if( $orderv->customer ){
                                        $name = ( $orderv->customer->first_name ) ? $orderv->customer->first_name : '';
                                        $name = ( $orderv->customer->last_name ) ? $name . ' ' .$orderv->customer->last_name : $name;
                                    }
                                    \Log::info('customer_name :: ' . $orderv->id);
                                    \Log::info($name);
                                    $order = ($o) ? $o : new Order;
                                    $order->user_id = $shop->id;
                                    $order->order_id = $orderv->id;
                                    $order->order_number = $orderv->order_number;
                                    $order->order_name = $orderv->name;
                                    $order->order_status = ($orderv->cancelled_at) ? 'canceled' : 'active';
                                    $order->customer_name = $name;
                                    $order->order_total = $orderv->total_price;
                                    $order->order_counter = $counter;
                                    $order->date = $val->value;
                                    $order->sh_order_created_date = date("Y-m-d H:i:s", strtotime($orderv->created_at));
                                    $order->sh_order_updated_date = date("Y-m-d H:i:s", strtotime($orderv->updated_at));
                                    $order->save();
                                }
                            }
                        }
                        $items = LineItems::where('user_id', $shop->id)->where('order_id', $orderv->id)->get();
                        foreach( $items as $key=>$val ){
                            $val->delete();
                        }
                        if($orderv->cancelled_at == null) {
                            if( !empty( $orderv->line_items ) ){
                                foreach ( $orderv->line_items as $lkey=>$lval ){
                                    //                        \Log::Info(json_encode($lval));

                                    if( $lval->product_id ){
                                        $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'. $lval->product_id .'.json';
                                        $parameter['fields'] = "id,product_type";

                                        $products = $shop->api()->rest('GET', $endPoint, $parameter);
                                        // \Log::info(json_encode($products));
                                        if( !$products->errors ){
                                            if ($products->body->product) {
                                                $product = $products->body->product;
                                                $product_type = $product->product_type;
                                                // \Log::info($product_type);
                                            }
                                        }
                                    }
                                    $properties = [];
                                    if( is_array($lval->properties) && !empty($lval->properties) ){
                                        foreach ( $lval->properties as $pkey=>$pval ){
                                            $properties[$pkey] = $pval->name . ': ' . $pval->value;
                                        }
                                    }else{
                                        $properties = '';
                                    }

                                    $lineitems = new LineItems;
                                    $lineitems->user_id = $shop->id;
                                    $lineitems->lineitem_id = $lval->id;
                                    $lineitems->order_id = $orderv->id;
                                    $lineitems->product_id = $lval->product_id;
                                    $lineitems->variant_id = $lval->variant_id;
                                    $lineitems->order_name = $orderv->name;
                                    $lineitems->product_name = $lval->title;
                                    $lineitems->product_type = $product_type;
                                    $lineitems->variant_name = $lval->variant_title;
                                    $lineitems->quantity = $lval->quantity;
                                    $lineitems->price = $lval->price;
                                    $lineitems->total = (int) $lval->quantity * (float) $lval->price;
                                    $lineitems->properties = ( is_array($properties) ) ? implode( " , ", $properties ) : '';
                                    $lineitems->date = $this->date;
                                    $lineitems->save();
                                }
                            }
                        }
                    }
                }
            }


        }catch(\Exception $e){
            \Log::info($e->getMessage());
        }
    }
}
