<?php

namespace App\Jobs;

use App\LineItems;
use App\Models\Customer;
use App\Models\Order;
use Osiset\ShopifyApp\Actions\CancelCurrentPlan;
use Osiset\ShopifyApp\Contracts\Commands\Shop as IShopCommand;
use Osiset\ShopifyApp\Contracts\Queries\Shop as IShopQuery;

class AppUninstalledJob extends \Osiset\ShopifyApp\Messaging\Jobs\AppUninstalledJob
{
    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle(
        IShopCommand $shopCommand,
        IShopQuery $shopQuery,
        CancelCurrentPlan $cancelCurrentPlanAction
    ): bool {
        \Log::info('------------------- App uninstalled JOB-------------------');
        // Get the shop
        \Log::info($shopQuery->getByDomain($this->domain));

        $shop = $shopQuery->getByDomain($this->domain);
        \Log::info($shopQuery->getByDomain($this->domain));
        $shop_id = $shop->id;
        $shopId = $shop->getId();
        // Cancel the current plan
        $cancelCurrentPlanAction($shopId);

        $this->deleteRecords($shop_id);

        // Purge shop of token, plan, etc.
        $shopCommand->clean($shopId);

        // Soft delete the shop.
        $shopCommand->softDelete($shopId);

        return true;
    }
    public function deleteRecords($shopId){
        Order::where('user_id', $shopId)->delete();
        LineItems::where('user_id', $shopId)->delete();

    }
}
