<?php

namespace App\Listeners;

use App\Events\CheckOrder;
use App\LineItems;
use App\Models\Order;
use App\Models\Webhook;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddOrder implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        \Log::info('debug Listener');
    }

    /**
     * Handle the event.
     *
     * @param  CheckOrder  $event
     * @return void
     */
    public function handle(CheckOrder $event)
    {
        $ids = $event->ids;
        $user = User::where('id', $ids['user_id'])->first();
        $wb_order = Webhook::where('id', $ids['entity_id'])->first();
        $sh_order = json_decode($wb_order->data);
        \Log::info(!empty($sh_order->note_attributes));
        if( !empty($sh_order->note_attributes) ){
                    // \Log::Info(json_encode($val));
            foreach ($sh_order->note_attributes as $key=>$val){
                if( $val->name == 'date' ){
                    $this->date = $val->value;
                    $o = Order::where('order_id', $sh_order->id)->where('user_id', $ids['user_id'])->first();

                    if( is_array($sh_order->line_items) && !empty($sh_order->line_items) ){
                        $counter = count($sh_order->line_items);
                    }else{
                        $counter = 0;
                    }

                    $name = ( $sh_order->customer ) ? ($sh_order->customer->first_name) ? $sh_order->customer->first_name : '' : '';
                    $name = ( $sh_order->customer ) ? ($sh_order->customer->last_name) ? $name . ' ' .$sh_order->customer->last_name : $name : $name;
                    \Log::info('customer_name');
                    \Log::info($name);
                    $order = ($o) ? $o : new Order;
                    $order->user_id = $ids['user_id'];
                    $order->order_id = $sh_order->id;
                    $order->order_number = $sh_order->order_number;
                    $order->order_name = $sh_order->name;
                    $order->order_status = ($sh_order->cancelled_at) ? 'canceled' : 'active';
                    $order->customer_name = $name;
                    $order->order_total = $sh_order->total_price;
                    $order->order_counter = $counter;
                    $order->date = $val->value;
                    $order->sh_order_created_date = date("Y-m-d H:i:s", strtotime($sh_order->created_at));
                    $order->sh_order_updated_date = date("Y-m-d H:i:s", strtotime($sh_order->updated_at));
                    $order->save();
                }
            }
            $items = LineItems::where('user_id', $ids['user_id'])->where('order_id', $sh_order->id)->get();
            foreach( $items as $key=>$val ){
                $val->delete();
            }
            if($sh_order->cancelled_at == null) {
                    if( !empty( $sh_order->line_items ) ){
                        foreach ( $sh_order->line_items as $lkey=>$lval ){
    //                        \Log::Info(json_encode($lval));

                            if( $lval->product_id ){
                                $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'. $lval->product_id .'.json';
                                $parameter['fields'] = "id,product_type";

                                $products = $user->api()->rest('GET', $endPoint, $parameter);
                                // \Log::info(json_encode($products));
                                if( !$products->errors ){
                                    if ($products->body->product) {
                                        $product = $products->body->product;
                                        $product_type = $product->product_type;
                                        // \Log::info($product_type);
                                    }
                                }
                            }
                            $properties = [];
                            if( is_array($lval->properties) && !empty($lval->properties) ){
                                foreach ( $lval->properties as $pkey=>$pval ){
                                    $properties[$pkey] = $pval->name . ': ' . $pval->value;
                                }
                            }else{
                                $properties = '';
                            }

                            $lineitems = new LineItems;
                            $lineitems->user_id = $ids['user_id'];
                            $lineitems->lineitem_id = $lval->id;
                            $lineitems->order_id = $sh_order->id;
                            $lineitems->product_id = $lval->product_id;
                            $lineitems->variant_id = $lval->variant_id;
                            $lineitems->order_name = $sh_order->name;
                            $lineitems->product_name = $lval->title;
                            $lineitems->product_type = $product_type;
                            $lineitems->variant_name = $lval->variant_title;
                            $lineitems->quantity = $lval->quantity;
                            $lineitems->price = $lval->price;
                            $lineitems->total = (int) $lval->quantity * (float) $lval->price;
                            $lineitems->properties = ( is_array($properties) ) ? implode( " , ", $properties ) : '';
                            $lineitems->date = $this->date;
                            $lineitems->save();
                        }
                    }
                }
            }

        $wb_order->is_executed = 1;
        $wb_order->save();
    }
}
