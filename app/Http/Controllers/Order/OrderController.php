<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\LineItems;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class OrderController extends Controller
{
    private $grandTotal = 0;
    public function index(Request $request){
        try{
            $user = Auth::user();
            if( $request->d != '' ) {
                $entities = Order::where('user_id', $user->id)->where('date', $request->d)->where('order_status', 'active')->orderBy('sh_order_created_date', 'desc')->paginate(10);
            }else{
                $entities = Order::where('user_id', $user->id)->where('order_status', 'active')->orderBy('sh_order_created_date', 'desc')->paginate(10);
            }

            if( $entities ){
                $entity = $entities->map(function ($name) use ($user) {
                    $this->grandTotal += $name->order_total;
                    return [
                        'order_id' => $name->order_id,
                        'order_number' => $name->order_number,
                        'customer_name' => $name->customer_name,
                        'order_total' => $name->order_total,
                        'order_counter' => $name->order_counter,
                        'date' => $name->date,
                        'order_url' => 'https://' . $user->name . '/admin/orders/' . $name->order_id,
                    ];
                })->toArray();
            }
            $data['data'] = $entity;
            $data['grandTotal'] = number_format($this->grandTotal, 2);
            $data['prev'] = ( $entities->previousPageUrl() ) ? $entities->previousPageUrl() : '';
            $data['next'] = ( $entities->nextPageUrl() ) ? $entities->nextPageUrl() : '';
            return response::json(['data' => $data ], 200);
        }catch(\Exception $e){
            return response::json(['data' => $e ], 422);
        }
    }

    public function lineItems(Request $request){
        try{
            $user = Auth::user();
            $entities = LineItems::select('product_type')->where('user_id', $user->id)->where('date', $request->d)->groupBy('product_type')->paginate(10);

            $product_types = $entities->map(function ($type) {
                return $type->product_type;
            })->toArray();

            $final_entity = [];

            foreach ( $product_types as $key=>$val  ){
                $fnt = [];
                $entity = LineItems::where('date', $request->d)->where('user_id', $user->id)->where('product_type', $val)->get();

                $product_ids = $entity->map(function ($name) {
                    return $name->product_id;
                })->toArray();

                if( $entity->count() > 0 ){
                    $fnt[] = $val;
                }
                $fn1 = [];
                foreach( $entity as $ekey=>$eval ){
                    $fn1[$eval->product_id]['product_name'] = $eval->product_name;
                    $fn1[$eval->product_id]['variant_name'] = $eval->variant_name;
                    $fn1[$eval->product_id]['quantity'] =  (array_key_exists("quantity",$fn1[$eval->product_id])) ? (int)$fn1[$eval->product_id]['quantity'] + $eval->quantity : $eval->quantity;
                    if((array_key_exists("properties",$fn1[$eval->product_id]))){
                        $fn1[$eval->product_id]['properties'] = $fn1[$eval->product_id]['properties'] . "\n" . str_replace(',', "\n", $eval->properties);
                    }else{
                        $fn1[$eval->product_id]['properties'] = str_replace(',', "\n", $eval->properties);
                    }
//                    $fn1[$eval->product_id]['properties'] = (array_key_exists("properties",$fn1[$eval->product_id])) ? $fn1[$eval->product_id]['properties'] .  $eval->properties : $eval->properties;
                }
                // $fn = $entity->map(function ($name) {
                //     return [
                //         'product_name' => $name->product_name,
                //         'variant_name' => $name->variant_name,
                //         'quantity' => $name->quantity,
                //         'properties' => $name->properties
                //     ];
                // })->toArray();
                $fn = array_merge($fnt, $fn1);
                $final_entity = array_merge($final_entity, $fn);
            }

            $data['data'] = $final_entity;
            $data['grandTotal'] = $this->grandTotal;
            $data['prev'] = ( $entities->previousPageUrl() ) ? $entities->previousPageUrl() : '';
            $data['next'] = ( $entities->nextPageUrl() ) ? $entities->nextPageUrl() : '';
            return response::json(['data' => $data ], 200);
        }catch(\Exception $e){
            return response::json(['data' => $e->getMessage() ], 422);
        }
    }

}
