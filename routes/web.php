<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify'])->name('home');

//Auth::routes();
Route::group(['middleware' => 'auth.shopify'], function () {
    Route::get("orders", 'Order\OrderController@index');
    Route::get("line-items", 'Order\OrderController@lineItems');
});

Route::get('flush', function(){
    request()->session()->flush();
});
