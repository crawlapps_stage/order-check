import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/Order/List').default,
        name:'orderlist',
        meta: {
            title: 'List',
            description: '',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/lineitemlist',
        component: require('../components/Order/LineItemList').default,
        name:'lineitemlist',
        meta: {
            title: 'LineItemList',
            description: '',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
];

const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});

export default router;
